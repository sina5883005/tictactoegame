public class Board{
	private Square[][] tictactoeBoard;
	
	//Square blank = Square.BLANK;
	
		public Board(){
		this.tictactoeBoard = new Square[3][3];
			for(int i = 0; i < tictactoeBoard.length ; i++){
				for(int j = 0; j < tictactoeBoard[i].length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
				}
			}
		}
		
	public String toString(){
		String boardTable = "";
		for(int i = 0; i < tictactoeBoard.length ; i++){
			for(int j = 0; j < tictactoeBoard[i].length; j++){
				boardTable += tictactoeBoard[i][j] + " " ;
			}
			boardTable += "\n";
		}
		return boardTable;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(row < 0 || row > 2 || col < 0 || col > 2){
			return false;
		}
		if(tictactoeBoard[row][col] == Square.BLANK){
			tictactoeBoard[row][col] = playerToken;
			return true;
		}else{
			return false;
		}
	}
	
	public boolean checkIfFull(){
		for(int row = 0; row < tictactoeBoard.length; row++){
			for(int col = 0; col < tictactoeBoard[row].length; col++){
				if(tictactoeBoard[row][col] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for(int row = 0; row < tictactoeBoard.length; row++){
			int count = 0;
			for(int col = 0; col < tictactoeBoard[row].length; col++){
				if (tictactoeBoard[row][col] == playerToken){
					count++;
				}
			}
			if(count == 3){
				return true;
			}
		}
		return false;
	} 
	
	private boolean checkIfWinningVertical(Square playerToken){
		for(int col = 0; col < 3; col++){
			if(tictactoeBoard[0][col] == playerToken && tictactoeBoard[1][col] == playerToken 
				&& tictactoeBoard[2][col] == playerToken){
				return true;
			}
		}
		return false;
	}

	public boolean checkIfWinning(Square playerToken){
		for(int row = 0; row < 3; row++){
			if(this.checkIfWinningHorizontal(playerToken)){
				return true;
			}
		}
		for(int col = 0; col < 3; col++){
			if(this.checkIfWinningVertical(playerToken)){
				return true;
			}
		}
		return false;
	}
}