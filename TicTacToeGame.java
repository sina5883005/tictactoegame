import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String[] args){
		System.out.println("Welcome to Tic Tac Toe :)");
		
		Scanner reader = new Scanner(System.in);
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		
		System.out.println(gameOver);
		
		while(!gameOver){
			System.out.println(board);
			
            if (player == 1) {
                playerToken = Square.X;
            } else {
                playerToken = Square.O;
            }

		    System.out.printf("Player " + player + ", enter a row (0-2): ");
            int row = reader.nextInt();
            System.out.printf("Player " + player + ", enter a column (0-2): ");
            int col = reader.nextInt();
			
			boolean a = board.placeToken(row, col, playerToken);
            while(!a){
                System.out.println("Invalid input. Please try again.");
                System.out.printf("Player " + player + ", enter a row (0-2): ");
                row = reader.nextInt();
                System.out.printf("Player " + player + ", enter a column (0-2): ");
                col = reader.nextInt();
                a = board.placeToken(row, col, playerToken);

            }
			
			if(board.checkIfWinning(playerToken)){
                System.out.printf("Player " + player + " wins!");
                gameOver = true;
            }else if(board.checkIfFull()){
                System.out.println("It's a tie!");
                gameOver = true;
            }else{
                player++;
                if(player > 2){
                    player = 1;
                }
            }
        }
	}
}